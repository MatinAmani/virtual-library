#include "pch.h"
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <sstream>
using namespace std;

//--------------------------------------------------------------------------------------------
//Sub-Functions box:
class Date
{
private:
	int year;
	int month;
	int day;
public:
	void ReadDate(int y, int m, int d) {
		year = y;
		month = m;
		day = d;
	}

	void PrintDate() {
		cout << year << "/" << month << "/" << day << endl;
	}

	void PutDate(char date[]) {
		int i;
		int Year = year;
		int Month = month;
		int Day = day;
		char y, m, d;

		for (i = 3;i >= 0;i--)
		{
			y = (Year % 10) + 48;
			date[i] = y;
			Year /= 10;
		}
		date[4] = '/';
		for (i = 6;i >= 5;i--)
		{
			m = (Month % 10) + 48;
			date[i] = m;
			Month /= 10;
		}
		date[7] = '/';
		for (i = 9;i >= 8;i--)
		{
			d = (Day % 10) + 48;
			date[i] = d;
			Day /= 10;
		}

	}

};

struct Book
{
	string title;
	string writer;
	int Rd, ISBN, count, avlbl;
	//(Rd : Release year | count : Numbers of this book that have been bought for the library | avlbl : Numbers available at the moment)
};

struct Member
{
	string name;
	string Lname;
	int borrow;
	long long stnu, ntnu;
	//(Lname : Last Name | borrow : Numbers of Borrowed Books | stnu : Studentary number | ntnu : National number)
};

struct LendBook
{
	int ISBN;
	long long stnu;
	Date BorrowD, ReturnD;
	//(stnu : Studentary Number | BorrowD : Borrow date | ReturnD : Return date)
};

int CharToIntYear(char ch[])
{
	int i, chr, sum = 0;
	for (i = 0;i < 4;i++)
	{
		chr = ch[i];
		sum += ((chr - 48) * 1) * (pow(10, 3 - i));
	}
	return(sum);
}

int CharToIntMonth(char ch[])
{
	int i, chr, sum = 0;
	for (i = 5;i < 7;i++)
	{
		chr = ch[i];
		sum += ((chr - 48) * 1) * (pow(10, 6 - i));
	}
	return(sum);
}

int CharToIntDay(char ch[])
{
	int i, chr, sum = 0;
	for (i = 8;i < 10;i++)
	{
		chr = ch[i];
		sum += ((chr - 48) * 1) * (pow(10, 9 - i));
	}
	return(sum);
}

Date ReturnDateCal(LendBook lb[], int i)
{
	Date Bdate, Rdate;
	char Bd[10];
	int y, m, d, dd;
	int ry, rm, rd;
	Bdate = lb[i].BorrowD;
	Bdate.PutDate(Bd);

	y = CharToIntYear(Bd);
	m = CharToIntMonth(Bd);
	d = CharToIntDay(Bd);
	dd = d;

	d += 7;

	if (d > 30)
	{
		m++;
		if (m > 12)
		{
			y++;
			m = 1;
		}
		d = 7 - (30 - dd);
	}

	ry = y;
	rm = m;
	rd = d;

	Rdate.ReadDate(ry, rm, rd);
	return(Rdate);
}

void PutInFile_Book(Book b[], int n)
{
	int i;
	fstream book_file;
	book_file.open("Books.dat");
	for (i = 0;i < n;i++)
	{
		book_file << b[i].title << endl;
		book_file << b[i].writer << endl;
		book_file << b[i].Rd << endl;
		book_file << b[i].ISBN << endl;
		book_file << b[i].count << endl;
		book_file << b[i].avlbl << endl;
	}
	book_file.close();
}

void ReadFile_Book(Book b[], int n)
{
	int i;
	string line;
	fstream book_file;
	book_file.open("Books.dat");
	for (i = 0;i < n;i++)
	{
		getline(book_file, b[i].title);
		getline(book_file, b[i].writer);
		getline(book_file, line);
		b[i].Rd = stoi(line);
		getline(book_file, line);
		b[i].ISBN = stoi(line);
		getline(book_file, line);
		b[i].count = stoi(line);
		getline(book_file, line);
		b[i].avlbl = stoi(line);
	}
	book_file.close();
}

void PutInFile_Member(Member m[], int n)
{
	int i;
	fstream member_file;
	member_file.open("Members.dat");
	for (i = 0;i < n;i++)
	{
		member_file << m[i].name << endl;
		member_file << m[i].Lname << endl;
		member_file << m[i].stnu << endl;
		member_file << m[i].ntnu << endl;
		member_file << m[i].borrow << endl;
	}
	member_file.close();
}

void ReadFile_Member(Member m[], int n)
{
	int i;
	string line;
	fstream member_file;
	member_file.open("Members.dat");
	for (i = 0;i < n;i++)
	{
		getline(member_file, m[i].name);
		getline(member_file, m[i].Lname);
		getline(member_file, line);
		m[i].stnu = stoll(line);
		getline(member_file, line);
		m[i].ntnu = stoll(line);
		getline(member_file, line);
		m[i].borrow = stoi(line);
	}
	member_file.close();
}

void PutInFile_LendBook(LendBook lb[], int n)
{
	int i, j;
	string line, d[10];
	fstream lendbook_file;
	char date[10];
	lendbook_file.open("Lended Books.dat");
	for (i = 0;i < n;i++)
	{
		lendbook_file << lb[i].ISBN << endl;

		lendbook_file << lb[i].stnu << endl;

		lb[i].BorrowD.PutDate(date);
		for (j = 0;j < 10;j++)
		{
			d[j] = date[j];
		}
		for (j = 0;j < 10;j++)
		{
			lendbook_file << d[j];
		}
		lendbook_file << endl;
		
		lb[i].ReturnD.PutDate(date);
		for (j = 0;j < 10;j++)
		{
			d[j] = date[j];
		}
		for (j = 0;j < 10;j++)
		{
			lendbook_file << d[j];
		}
		lendbook_file << endl;
	}
	lendbook_file.close();
}

void ReadFile_LendBook(LendBook lb[], int n)
{
	int i, j;
	int y, m, d;
	int ry, rm, rd;
	char Bdate[10], Rdate[10];
	string line;
	fstream lendbook_file;

	lendbook_file.open("Lended Books.dat");
	for (i = 0;i < n;i++)
	{
		getline(lendbook_file, line);
		lb[i].ISBN = stoi(line);

		getline(lendbook_file, line);
		lb[i].stnu = stoll(line);

		getline(lendbook_file, line);
		for (j = 0;j < 10;j++)
		{
			Bdate[j] = line[j];
		}
		y = CharToIntYear(Bdate);
		m = CharToIntMonth(Bdate);
		d = CharToIntDay(Bdate);
		lb[i].BorrowD.ReadDate(y, m, d);

		getline(lendbook_file, line);
		for (j = 0;j < 10;j++)
		{
			Rdate[j] = line[j];
		}
		ry = CharToIntYear(Rdate);
		rm = CharToIntMonth(Rdate);
		rd = CharToIntDay(Rdate);
		lb[i].ReturnD.ReadDate(ry, rm, rd);
	}
	lendbook_file.close();
}

int SearchBook_ByISBN(Book b[], int isbn)
{
	int i;
	for (i = 0;i < 1000;i++)
	{
		if (b[i].ISBN == isbn)
		{
			return(i);
		}
	}
	return(-1);
}

int SearchBook_ByTitle(Book b[], string str)
{
	int i;
	for (i = 0;i < 1000;i++)
	{
		if (b[i].title == str)
		{
			return(i);
		}
	}
	return(-1);
}

int SearchBook_ByWriter(Book b[], string str, int n)
{
	int i;
	for (i = n;i < 1000;i++)
	{
		if (b[i].writer == str)
		{
			return(i);
		}
	}
	return(-1);
}

bool SearchResultByISBN_Book(Book b[], int isbn, int n)
{
	bool Sresult;
	if (SearchBook_ByISBN(b, isbn) == -1)
	{
		Sresult = false;
	}
	else
	{
		Sresult = true;
	}
	return(Sresult);
}

int SearchMember_ByNtnu(Member m[], long long nn)
{
	int i;
	for (i = 0;i < 100;i++)
	{
		if (m[i].ntnu == nn)
		{
			return(i);
		}
	}
	return(-1);
}

int SearchMember_ByLName(Member m[], string str)
{
	int i;
	for (i = 0;i < 100;i++)
	{
		if (m[i].Lname == str)
		{
			return(i);
		}
	}
	return(-1);
}

int SearchMember_ByStnu(Member m[], long long sn)
{
	int i;
	for (i = 0;i < 100;i++)
	{
		if (m[i].stnu == sn)
		{
			return(i);
		}
	}
	return(-1);
}

int SearchLendBook_ByISBN_Stnu(LendBook lb[], int isbn, long long sn)
{
	int i;
	for (i = 0;i < 1000;i++)
	{
		if (lb[i].ISBN == isbn and lb[i].stnu == sn)
		{
			return(i);
		}
	}
	return(-1);
}

void ReadInfo_Book(Book b[], int n)
{
	cout << "Title: "; cin >> b[n].title;
	cout << "Writer: "; cin >> b[n].writer;
	cout << "Release Year: "; cin >> b[n].Rd;
	cout << "Numbers Bought for Library: "; cin >> b[n].count;
	b[n].avlbl = b[n].count;
}

void PrintInfo_Book(Book b[], int i)
{
	cout << b[i].title << endl;
	cout << "By " << b[i].writer << endl;
	cout << "Published in " << b[i].Rd << endl;
	cout << "ISBN: " << b[i].ISBN << endl;
	cout << "Inventory: " << b[i].count << endl;
	cout << "Available at the Moment: " << b[i].avlbl << endl;
}

void ReadInfo_Member(Member m[], int i)
{
	cout << "Name: "; cin >> m[i].name;
	cout << "Last Name: "; cin >> m[i].Lname;
	cout << "Studentary Number: "; cin >> m[i].stnu;
}

void PrintInfo_Member(Member m[], int i)
{
	cout << "Name: " << m[i].name << " " << m[i].Lname << endl;
	cout << "Studentary Number: " << m[i].stnu << endl;
	cout << "National Number: " << m[i].ntnu << endl;
	cout << m[i].borrow << " Book(s) Borrowed" << endl;
}

void PrintInfo_LendBook(LendBook lb[], int i)
{
	cout <<"ISBN: "<< lb[i].ISBN << endl;
	cout <<"Borrowed By: "<< lb[i].stnu << endl;
	cout << "Borrow Date: ";
	lb[i].BorrowD.PrintDate();
	cout << "Return Date: ";
	lb[i].ReturnD.PrintDate();
}

int BookCounter()
{
	int count = 0;
	string line;
	fstream book_file;
	book_file.open("Books.dat");
	while (!book_file.eof())
	{
		getline(book_file, line);
		count++;
	}
	count /= 6;
	return(count);
}

int MemberCounter()
{
	int count = 0;
	string line;
	fstream member_file;
	member_file.open("Members.dat");
	while (!member_file.eof())
	{
		getline(member_file, line);
		count++;
	}
	count /= 5;
	return(count);
}

int LendBookCounter()
{
	int count = 0;
	string line;
	fstream lendbook_file;
	lendbook_file.open("Lended Books.dat");
	while (!lendbook_file.eof())
	{
		getline(lendbook_file, line);
		count++;
	}
	count /= 4;
	return(count);
}

//--------------------------------------------------------------------------------------------


void main()
{
	//--------------------------------------------------------------------------------------------
	//Variables box:

	Date today;

	Book book[1000];
	Member member[100];
	LendBook lendbook[1000];

	fstream book_file, member_file, lendbook_file;

	bool condition;

	int i, j, k, l; //counters
	int y, yr, m, mth, d, dy; //(y : Year | m : Month | d : Day)
	int ry, rm, rd; //(ry : Return Year | rm : Return Month | rd : Return Day)
	int ans1, ans2, ans3;
	int countbook, countmember, countlend, countdate, isbn;
	long long nn, sn; //(nn : National Number | sn : Studentary Number)
	string date_string, srch; //(srch : Search)
	char date[10];

	//--------------------------------------------------------------------------------------------

	//--------------------------------------------------------------------------------------------
	//Program box:

	countbook = BookCounter();
	countmember = MemberCounter();
	countlend = LendBookCounter();

	while (true)
	{
		cout << "Enter Today's Date(yyyy/mm/dd): ";
		cin >> date_string;

		for (i = 0;i < 10;i++)
		{
			date[i] = date_string[i];
		}

		y = CharToIntYear(date);
		m = CharToIntMonth(date);
		d = CharToIntDay(date);

		if ((y < 1300) or (m > 12) or (m < 1) or (d > 31) or (d < 1))
		{
			cout << "Invalid Date!" << endl;
			cout << "---------------------------" << endl;
		}

		else
		{
			today.ReadDate(y, m, d);
			break;
		}
	}
	//Date Entered

	book_file.open("Books.dat");
	ReadFile_Book(book, countbook);
	book_file.close();

	member_file.open("Members.dat");
	ReadFile_Member(member, countmember);
	member_file.close();

	lendbook_file.open("Lended Books.dat");
	ReadFile_LendBook(lendbook, countlend);
	lendbook_file.close();
	//Files Readed And Closed

	while(true)
	{
		system("cls");
		cout << "Date: ";
		today.PrintDate();
		cout << "WELCOME TO VIRTUAL LIBRARY" << endl;
		cout << "(Presenting " << countbook << " Titles With " << countmember << " Active Members)" << endl << endl;
		cout << "1. Add Book" << endl;
		cout << "2. Edit Book Records" << endl;
		cout << "3. Add Member" << endl;
		cout << "4. Lend a Book" << endl;
		cout << "5. Return a Book" << endl;
		cout << "6. Search" << endl;
		cout << "7. Report" << endl;
		cout << "8. Exit" << endl;
		cin >> ans1;

		if (ans1 == 1)
		{
			system("cls");
			cout << "ISBN: "; cin >> isbn;
			if (SearchBook_ByISBN(book, isbn) == -1)
			{
				cout << "Book Does Not Exist" << endl << endl;
				book[countbook].ISBN = isbn;
				ReadInfo_Book(book, countbook);
				countbook++;
				cout << "Book Added!";
				cin.get();
				cin.ignore();
			}
			else
			{
				cout << "Book is Available." << endl << endl;
				i = SearchBook_ByISBN(book, isbn);
				book[i].avlbl++;
				book[i].count++;
				PrintInfo_Book(book, i);
				cout << endl;
				cout << "One Added to Library." << endl;
				cin.get();
				cin.ignore();
			}
		}
		//Done!

		else if (ans1 == 2)
		{
			system("cls");
			cout << "Enter an ISBN to Change it's Records: ";
			cin >> isbn;
			if (SearchBook_ByISBN(book, isbn) == -1)
			{
				cout << "!ERROR!" << endl;
				cout << "Book Not Found!" << endl;
				cin.get();
				cin.ignore();
			}
			else
			{
				i = SearchBook_ByISBN(book, isbn);
				book[i].ISBN = isbn;
				cout << "Enter New Records" << endl << endl;
				ReadInfo_Book(book, i);
				cout << "Available at the Moment: "; cin >> book[i].avlbl;
			}

		}
		//Done!

		else if (ans1 == 3)
		{
			system("cls");
			cout << "National Number: "; cin >> nn;
			if (SearchMember_ByNtnu(member, nn) == -1)
			{
				member[countmember].ntnu = nn;
				ReadInfo_Member(member, countmember);
				member[countmember].borrow = 0;
				countmember++;
				cout << "Member Added!";
				cin.get();
				cin.ignore();
			}
			else
			{
				system("cls");
				cout << "Member with national number: " << nn << " Exists." << endl << endl;
				i = SearchMember_ByNtnu(member, nn);
				PrintInfo_Member(member, i);
				cin.get();
				cin.ignore();
			}
		}
		//Done!

		else if (ans1 == 4)
		{
			system("cls");
			cout << "Enter Borrower's Studentary Number: "; cin >> sn;
			if (SearchMember_ByStnu(member, sn) == -1)
			{
				cout << "Student with ID " << sn << " is Not a member of this library!" << endl;
				cout << "Add to Library?(1/0) "; cin >> ans2;
				if (ans2 == 1)
				{
					cout << "National Number: "; cin >> nn;
					member[countmember].ntnu = nn;
					ReadInfo_Member(member, countmember);
					member[countmember].borrow = 0;
					countmember++;
					cout << "Member Added!";
					cin.get();
					cin.ignore();
				}
			}

			else
			{
				i = SearchMember_ByStnu(member, sn);
				if (member[i].borrow < 3)
				{
					system("cls");
					cout << "1. Search Book By Title" << endl;
					cout << "2. Search Book By ISBN" << endl;
					cin >> ans2;

					if (ans2 == 1)
					{
						system("cls");
						cout << "Search By Title" << endl << endl;
						cout << "Title: "; cin >> srch;
						if (SearchBook_ByTitle(book, srch) == -1)
						{
							cout << "Book Not Found";
							cin.get();
							cin.ignore();
						}
						else
						{
							j = SearchBook_ByTitle(book, srch);
							if (book[j].avlbl > 0)
							{
								PrintInfo_Book(book, j);
								cout << endl;
								cout << "Are You Sure?(1/0) "; cin >> ans3;
								if (ans3 == 1)
								{
									isbn = book[j].ISBN;
									book[j].avlbl--;
									member[i].borrow++;
									lendbook[countlend].ISBN = isbn;
									lendbook[countlend].stnu = sn;
									lendbook[countlend].BorrowD = today;
									lendbook[countlend].ReturnD = ReturnDateCal(lendbook, countlend);
									countlend++;
									cout << "Book Lended!";
									cin.get();
									cin.ignore();
								}
							}
							else
							{
								PrintInfo_Book(book, j);
								cout << endl;
								cout << "!ERROR!" << endl;
								cout << "Insufficient Inventory!";
								cin.get();
								cin.ignore();
							}
						}
					}
					else if (ans2 == 2)
					{
						system("cls");
						cout << "Enter ISBN: "; cin >> isbn;
						if (SearchBook_ByISBN(book, isbn) == -1)
						{
							cout << "Book Not Exist!";
							cin.get();
							cin.ignore();
						}
						else
						{
							j = SearchBook_ByISBN(book, isbn);
							if (book[j].avlbl > 0)
							{
								PrintInfo_Book(book, j);
								cout << endl;
								cout << "Are You Sure?(1/0) "; cin >> ans3;
								if (ans3 == 1)
								{
									book[j].avlbl--;
									member[i].borrow++;
									lendbook[countlend].ISBN = isbn;
									lendbook[countlend].stnu = sn;
									lendbook[countlend].BorrowD = today;
									lendbook[countlend].ReturnD = ReturnDateCal(lendbook, countlend);
									countlend++;
									cout << "Book Lended!";
									cin.get();
									cin.ignore();
								}
							}
							else
							{
								PrintInfo_Book(book, j);
								cout << endl;
								cout << "!ERROR!" << endl;
								cout << "Insufficient Inventory!";
								cin.get();
								cin.ignore();
							}
							
						}
						
					}
				}

				else
				{
					cout << "This Member is Not Alowed To Borrow Books!" << endl;
					cout << "Borrowing Limit Reached" << endl;
					cin.get();
					cin.ignore();
				}

			}
		}
		//Done!

		else if (ans1 == 5)
		{
			system("cls");
			cout << "ISBN: "; cin >> isbn;
			cout << "Studentary Number: "; cin >> sn;
			if (SearchLendBook_ByISBN_Stnu(lendbook, isbn, sn) == -1)
			{
				cout << "!ERROR!" << endl;
				cout << "The Given Details Don't Match" << endl;
				cout << "Book Was Not Borrowed By This Student" << endl;
				cin.get();
				cin.ignore();
			}
			else
			{
				i = SearchLendBook_ByISBN_Stnu(lendbook, isbn, sn);

				j = lendbook[i].ISBN;
				k = SearchBook_ByISBN(book, j);
				book[k].avlbl++;

				j = lendbook[i].stnu;
				k = SearchMember_ByStnu(member, j);
				member[k].borrow--;

				today.PutDate(date);
				y = CharToIntYear(date);
				m = CharToIntMonth(date);
				d = CharToIntDay(date);

				lendbook[i].ReturnD.PutDate(date);
				ry = CharToIntYear(date);
				rm = CharToIntMonth(date);
				rd = CharToIntDay(date);

				if (y > ry or m > rm or (y == ry and m == rm and d > rd) or (y == ry and d == rd and m > rm) or (m == rm and d == rd and y > ry))
				{
					cout << "Warning" << endl;
					cout << "Book Has Been Returned AFTER Deadline" << endl;
				}
				cout << "Book Added Back to Library!";

				for (l = i;l < countlend - 1;l++)
				{
					lendbook[l] = lendbook[l + 1];
				}

				countlend--;
				cin.get();
				cin.ignore();
			}
		}
		//Done!

		else if (ans1 == 6)
		{
			system("cls");
			cout << "1. Among Books" << endl;
			cout << "2. Among Members" << endl;
			cin >> ans2;

			if (ans2 == 1)
			{
				system("cls");
				cout << "1. Search By Title" << endl;
				cout << "2. Search By Writer" << endl;
				cout << "3. Search By ISBN" << endl;
				cin >> ans3;

				if (ans3 == 1)
				{
					system("cls");
					cout << "Search By Title" << endl << endl;
					cout << "Enter Title: "; cin >> srch;
					if (SearchBook_ByTitle(book, srch) == -1)
					{
						cout << srch << " Not Found!";
						cin.get();
						cin.ignore();
					}
					else
					{
						i = SearchBook_ByTitle(book, srch);
						cout << "Result:" << endl;
						PrintInfo_Book(book, i);
						cin.get();
						cin.ignore();
					}
				}

				else if (ans3 == 2)
				{
					system("cls");
					cout << "Search By Writer" << endl << endl;
					cout << "Enter Writer's Name: "; cin >> srch;
					if (SearchBook_ByWriter(book, srch, 0) == -1)
					{
						cout << "No Books By " << srch << " Exist.";
						cin.get();
						cin.ignore();
					}
					else
					{
						j = 0;
						while (SearchBook_ByWriter(book, srch, j) != -1)
						{
							i = SearchBook_ByWriter(book, srch, j);
							PrintInfo_Book(book, i);
							cout << endl;
							j = i + 1;
						}
						cin.get();
						cin.ignore();
					}
				}

				else if (ans3 == 3)
				{
					system("cls");
					cout << "Search By ISBN" << endl << endl;
					cout << "Enter ISBN: "; cin >> isbn;
					if (SearchBook_ByISBN(book, isbn) == -1)
					{
						cout << "No Books Found!";
						cin.get();
						cin.ignore();
					}
					else
					{
						i = SearchBook_ByISBN(book, isbn);
						PrintInfo_Book(book, i);
						cin.get();
						cin.ignore();
					}
				}
			}

			else if (ans2 == 2)
			{
				system("cls");
				cout << "1. Search By Last Name" << endl;
				cout << "2. Search By Studentary Number" << endl;
				cout << "3. Search By National Code" << endl;
				cin >> ans3;

				if (ans3 == 1)
				{
					system("cls");
					cout << "Search By Last Name" << endl << endl;
					cout << "Enter Last Name: "; cin >> srch;
					if (SearchMember_ByLName(member, srch) == -1)
					{
						cout << "Member Not Found!";
						cin.get();
						cin.ignore();
					}
					else
					{
						i = SearchMember_ByLName(member, srch);
						PrintInfo_Member(member, i);
						cin.get();
						cin.ignore();
					}

				}

				else if (ans3 == 2)
				{
					system("cls");
					cout << "Search By Studentary Number" << endl << endl;
					cout << "Enter Studentary Number: "; cin >> sn;
					if (SearchMember_ByStnu(member, sn) == -1)
					{
						cout << "Member Not Found!";
						cin.get();
						cin.ignore();
					}
					else
					{
						i = SearchMember_ByStnu(member, sn);
						PrintInfo_Member(member, i);
						cin.get();
						cin.ignore();
					}
				}

				else if (ans3 == 3)
				{
					system("cls");
					cout << "Search By National Number" << endl << endl;
					cout << "Enter National Number: "; cin >> nn;
					if (SearchMember_ByNtnu(member, nn) == -1)
					{
						cout << "Member Not Found!";
						cin.get();
						cin.ignore();
					}
					else
					{
						i = SearchMember_ByNtnu(member, nn);
						PrintInfo_Member(member, i);
						cin.get();
						cin.ignore();
					}
				}
			}
		}
		//Done!

		else if (ans1 == 7)
		{
			system("cls");
			cout << "1. Bookshelf" << endl;
			cout << "2. Members Details" << endl;
			cout << "3. All Lended Books" << endl;
			cin >> ans2;

			if (ans2 == 1)
			{
				system("cls");
				for (i = 0;i < countbook;i++)
				{
					cout << i + 1 << ":" << endl;
					PrintInfo_Book(book, i);
					cout << endl;
				}
				cin.get();
				cin.ignore();
			}
			else if (ans2 == 2)
			{
				system("cls");
				for (i = 0;i < countmember;i++)
				{
					cout << i + 1 << ":" << endl;
					PrintInfo_Member(member, i);
					cout << endl;
				}
				cin.get();
				cin.ignore();
			}
			else if (ans2 == 3)
			{
				system("cls");
				for (i = 0;i < countlend;i++)
				{
					cout << i + 1 << ":" << endl;
					PrintInfo_LendBook(lendbook, i);
					cout << endl;
				}
				cin.get();
				cin.ignore();
			}
		}
		//Done!

		else if (ans1 == 8)
		{
			book_file.open("Books.dat");
			PutInFile_Book(book, countbook);
			book_file.close();

			member_file.open("Members.dat");
			PutInFile_Member(member, countmember);
			member_file.close();

			lendbook_file.open("Lended Books.dat");
			PutInFile_LendBook(lendbook, countlend);
			lendbook_file.close();

			break;
		}
		//Done!
	}
	//--------------------------------------------------------------------------------------------
}